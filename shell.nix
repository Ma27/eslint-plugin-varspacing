{ nixpkgs ? fetchTarball https://github.com/NixOS/nixpkgs/archive/8c1977c54a9671c73783d729c9d860b217147ccf.tar.gz
}:

with import nixpkgs { };

mkShell {
  name = "eslint-dev-env";
  buildInputs = [
    nodejs
    yarn
  ];
}
