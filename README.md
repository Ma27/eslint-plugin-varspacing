# eslint-plugin-varspacing

[![pipeline status](https://gitlab.com/Ma27/eslint-plugin-varspacing/badges/master/pipeline.svg)](https://gitlab.com/Ma27/eslint-plugin-varspacing/commits/master)
[![npm version](https://badge.fury.io/js/eslint-plugin-varspacing.svg)](https://www.npmjs.com/package/eslint-plugin-varspacing)
[![Build Status](https://codecov.io/gl/Ma27/eslint-plugin-varspacing/branch/master/graph/badge.svg)](https://codecov.io/gl/Ma27/eslint-plugin-varspacing/branch/master/)

This plugin ships a simple and fixable [ESLint](https://eslint.org) rule to check if variable declarations are
properly aligned.

## Installation

In order to activate the plugin the following options must be present in `.eslintrc`:

``` json
{
  "plugins": [
    "varspacing"
  ],
  "extends": "plugin:varspacing/recommended"
}
```

Please refer to the [var-spacing docs](https://gitlab.com/Ma27/eslint-plugin-varspacing/blob/master/docs/rules/var-spacing.md)
for further details about the ruleset.

## Hacking

It's recommended to use the [`nix`](https://nixos.org/nix/) package manager for
development purposes.

A customized developer shell can be opened like this:

```
$ nix-shell
[nix-shell]$ yarn
[nix-shell]$ yarn test
```

## License

This module is published under MIT license.
Please refer to the [`LICENSE`](https://github.com/Sententiaregum/eslint-plugin-varspacing/blob/master/LICENSE)
that was distributed with this package for further legal information.
