### Description

Describe what happened (ideally with error messages and the code snippet which caused the issue).

### Expected behavior

If the plugin misbehaved (e.g. a snippet was wrongly declared as error or
the autofixer misformatted your code)

### Additional information

* NodeJS & NPM version:
* ESLint version:
* Version of this plugin:
* Anything else worth to mention?
